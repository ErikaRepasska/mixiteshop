package food.nut;

/**
 * Created by Erika on 14-Mar-17.
 */
public class Almond extends Nut {

    private static final double PRICE = 0.89;
    private static final String NAME = "mandle";

    public Almond() {
        super(NAME, PRICE);
    }
}
