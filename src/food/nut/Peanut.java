package food.nut;

/**
 * Created by Erika on 14-Mar-17.
 */
public class Peanut extends Nut {

    private static final double PRICE = 0.39;
    private static final String NAME = "arašidy";

    public Peanut() {
        super(NAME, PRICE);
    }
}
