package food.nut;

import food.BaseFood;

/**
 * Created by Erika on 14-Mar-17.
 */
public abstract class Nut extends BaseFood {

    Nut(final String name, final double price) {
        super(name, price);
    }

    @Override
    public int getWeight() {
        return 20;
    }
}
