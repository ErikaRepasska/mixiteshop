package food.nut;

/**
 * Created by Erika on 14-Mar-17.
 */
public class Cashew extends Nut {

    private static final double PRICE = 0.99;
    private static final String NAME = "kešu";

    public Cashew() {
        super(NAME, PRICE);
    }
}
