package food.nut;

/**
 * Created by Erika on 14-Mar-17.
 */
public class Hazelnut extends Nut {

    private static final double PRICE = 1.09;
    private static final String NAME = "lieskové oriešky";

    public Hazelnut() {
        super(NAME, PRICE);
    }
}
