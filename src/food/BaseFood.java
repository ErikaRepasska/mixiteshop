package food;

import list.BaseCell;
import utils.Utils;

/**
 * Created by Erika on 14-Mar-17.
 */
public abstract class BaseFood extends BaseCell {

    private final double price;

    /**
     * Abstraktná metóda, ktorá musí byť implementovaná v triedach extendujúcich BaseFood
     * Priradzuje hmotnosť, charakteristickú pre danú skupinu surovín
     * @return hmotnosť danej skupiny surovín
     */
    public abstract int getWeight();

    protected BaseFood(final String name, final double price) {
        super(name);

        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public String getNameWithWeight() {
        return getDisplayName() + " (" + getWeight() + Utils.WEIGHT_UNIT + ")";
    }
}
