package food.baseline;

/**
 * Created by Erika on 14-Mar-17.
 */
public class SoyaFlakes extends BaselineFood {

    private static final double PRICE = 2.89;
    private static final String NAME = "sójové vločky";

    public SoyaFlakes() {
        super(NAME, PRICE);
    }
}
