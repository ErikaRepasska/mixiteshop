package food.baseline;

/**
 * Created by Erika on 14-Mar-17.
 */
public class CornFlakes extends BaselineFood {

    private static final double PRICE = 2.39;
    private static final String NAME = "kukuričné lupienky";

    public CornFlakes() {
        super(NAME, PRICE);
    }
}
