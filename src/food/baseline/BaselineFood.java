package food.baseline;

import food.BaseFood;

/**
 * Created by Erika on 14-Mar-17.
 */
public abstract class BaselineFood extends BaseFood {

    BaselineFood(final String name, final double price) {
        super(name, price);
    }

    @Override
    public int getWeight() {
        return 100;
    }
}
