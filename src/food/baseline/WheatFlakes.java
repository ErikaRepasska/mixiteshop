package food.baseline;

/**
 * Created by Erika on 14-Mar-17.
 */
public class WheatFlakes extends BaselineFood {

    private static final double PRICE = 2.79;
    private static final String NAME = "pšeničné lupienky";

    public WheatFlakes() {
        super(NAME, PRICE);
    }
}
