package food.baseline;

/**
 * Created by Erika on 14-Mar-17.
 */
public class OatFlakes extends BaselineFood {

    private static final double PRICE = 2.65;
    private static final String NAME = "ovsené vločky";

    public OatFlakes(){
        super(NAME, PRICE);
    }
}
