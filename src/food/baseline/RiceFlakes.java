package food.baseline;

/**
 * Created by Erika on 14-Mar-17.
 */
public class RiceFlakes extends BaselineFood {

    private static final double PRICE = 2.79;
    private static final String NAME = "ryžové vločky";

    public RiceFlakes() {
        super(NAME, PRICE);
    }
}
