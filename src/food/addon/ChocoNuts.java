package food.addon;

/**
 * Created by Erika on 14-Mar-17.
 */
public class ChocoNuts extends AddOn {

    private static final double PRICE = 0.79;
    private static final String NAME = "čoko-oriešky";

    public ChocoNuts() {
        super(NAME, PRICE);
    }
}
