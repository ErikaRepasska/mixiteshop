package food.addon;

/**
 * Created by Erika on 14-Mar-17.
 */
public class MilkChoco extends AddOn {

    private static final double PRICE = 0.59;
    private static final String NAME = "mliečna čokoláda";

    public MilkChoco() {
        super(NAME, PRICE);
    }
}
