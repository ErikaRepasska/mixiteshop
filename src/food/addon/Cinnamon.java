package food.addon;

/**
 * Created by Erika on 14-Mar-17.
 */
public class Cinnamon extends AddOn {

    private static final double PRICE = 0.29;
    private static final String NAME = "škorica";

    public Cinnamon() {
        super(NAME, PRICE);
    }
}
