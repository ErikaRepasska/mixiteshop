package food.addon;

/**
 * Created by Erika on 14-Mar-17.
 */
public class Coconut extends AddOn {

    private static final double PRICE = 0.49;
    private static final String NAME = "kokos";

    public Coconut() {
        super(NAME, PRICE);
    }
}
