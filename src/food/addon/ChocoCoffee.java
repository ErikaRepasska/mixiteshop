package food.addon;

/**
 * Created by Erika on 14-Mar-17.
 */
public class ChocoCoffee extends AddOn {

    private static final double PRICE = 0.69;
    private static final String NAME = "čoko-kávové guličky";

    public ChocoCoffee() {
        super(NAME, PRICE);
    }
}
