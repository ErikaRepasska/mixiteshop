package food.addon;

import food.BaseFood;

/**
 * Created by Erika on 14-Mar-17.
 */
public abstract class AddOn extends BaseFood {

    AddOn(final String name, final double price) {
        super(name, price);
    }

    @Override
    public int getWeight() {
        return 10;
    }
}
