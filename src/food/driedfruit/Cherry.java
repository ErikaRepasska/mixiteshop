package food.driedfruit;

/**
 * Created by Erika on 14-Mar-17.
 */
public class Cherry extends DriedFruit {

    private static final double PRICE = 0.85;
    private static final String NAME = "čerešne";

    public Cherry() {
        super(NAME, PRICE);
    }
}
