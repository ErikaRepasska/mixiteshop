package food.driedfruit;

/**
 * Created by Erika on 14-Mar-17.
 */
public class Strawberry extends DriedFruit {

    private static final double PRICE = 0.99;
    private static final String NAME = "jahody";

    public Strawberry() {
        super(NAME, PRICE);
    }
}
