package food.driedfruit;

/**
 * Created by Erika on 14-Mar-17.
 */
public class Banana extends DriedFruit {

    private static final double PRICE = 0.89;
    private static final String NAME = "banány";

    public Banana() {
        super(NAME, PRICE);
    }
}
