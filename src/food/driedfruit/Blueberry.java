package food.driedfruit;

/**
 * Created by Erika on 14-Mar-17.
 */
public class Blueberry extends DriedFruit {

    private static final double PRICE = 1.49;
    private static final String NAME = "čučoriedky";

    public Blueberry() {
        super(NAME, PRICE);
    }
}
