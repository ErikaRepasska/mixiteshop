package food.driedfruit;

/**
 * Created by Erika on 14-Mar-17.
 */
public class Pineapple extends DriedFruit {

    private static final double PRICE = 1.19;
    private static final String NAME = "ananás";

    public Pineapple() {
        super(NAME, PRICE);
    }
}
