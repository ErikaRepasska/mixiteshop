package food.driedfruit;

import food.BaseFood;

/**
 * Created by Erika on 14-Mar-17.
 */
public abstract class DriedFruit extends BaseFood {

    DriedFruit(final String name, final double price) {
        super(name, price);
    }

    @Override
    public int getWeight() {
        return 30;
    }
}
