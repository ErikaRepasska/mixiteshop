package listeners;

/**
 * Sleduje správanie súvisiase s Listami
 */
public interface ListChangedListener {
    /**
     * Zaznamenáva, kedy dôjde k zmene atribútu v liste
     */
    void onListChanged();
}
