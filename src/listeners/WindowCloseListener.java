package listeners;

public interface WindowCloseListener {
    /**
     * Zaznamenáva, kedy dôjde k zatvoreniu okna v GUI
     */
    void onChildWindowClosed();
}
