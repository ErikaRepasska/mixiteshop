package transport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Táto trieda napĺňa a vypisuje list miest doručenia, do ktorých je možné uskutočniť objednávky
 */
public class DeliveryPoints {

    private final ObservableList<City> deliveryPoints = FXCollections.observableArrayList();

    public DeliveryPoints() {
        deliveryPoints.add(new City("Bratislava"));
        deliveryPoints.add(new City("Trnava"));
        deliveryPoints.add(new City("Nitra"));
        deliveryPoints.add(new City("Trencin"));
        deliveryPoints.add(new City("Zilina"));
        deliveryPoints.add(new City("Banska Bystrica"));
        deliveryPoints.add(new City("Martin"));
        deliveryPoints.add(new City("Ruzomberok"));
        deliveryPoints.add(new City("Liptovsky Mikulas"));
        deliveryPoints.add(new City("Poprad"));
        deliveryPoints.add(new City("Spisska Nova Ves"));
        deliveryPoints.add(new City("Presov"));
        deliveryPoints.add(new City("Kosice"));
        deliveryPoints.add(new City("Humenne"));
    }

    public ObservableList<City> getDeliveryPoints() {
        return deliveryPoints;
    }
}