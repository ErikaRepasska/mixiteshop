package transport.deliverytypes;

/**
 * Created by Erika on 29-Apr-17.
 */
public class SvihajSuhaj extends BaseVehicle {

    private static final String NAME = "Švihaj Šuhaj";
    private static final int DELIVERY_TIME = 1;
    private static final int MAX_VEHICLE_CAPACITY = 5000;
    private final VehicleLocation mVehicleLocation;

    public SvihajSuhaj(final VehicleLocation vehicleLocation) {
        super(NAME + vehicleLocation);
        mVehicleLocation = vehicleLocation;
    }

    @Override
    public int getDeliveryTime() {
        return DELIVERY_TIME;
    }

    @Override
    public int getMaxVehicleCapacity(){
       return MAX_VEHICLE_CAPACITY;
    }

    @Override
    public VehicleLocation getVehicleLocation() {
        return mVehicleLocation;
    }
}
