package transport.deliverytypes;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Slúži na naplnenie a výpis listov vozidiel
 * Reprezentuje vozový park, ktorý má eshop k dispozícii
 */
public class Garage {

    private final ObservableList<BaseVehicle> vehicleList = FXCollections.observableArrayList();

    public Garage() {
        fillVehicleList();
    }

    private void fillVehicleList() {
        vehicleList.add(new Car(BaseVehicle.VehicleLocation.WEST));
        vehicleList.add(new Car(BaseVehicle.VehicleLocation.MIDDLE));
        vehicleList.add(new Car(BaseVehicle.VehicleLocation.EAST));
        vehicleList.add(new Van(BaseVehicle.VehicleLocation.WEST));
        vehicleList.add(new Van(BaseVehicle.VehicleLocation.MIDDLE));
        vehicleList.add(new Van(BaseVehicle.VehicleLocation.EAST));
        vehicleList.add(new SvihajSuhaj(BaseVehicle.VehicleLocation.WEST));
        vehicleList.add(new Post(BaseVehicle.VehicleLocation.WHOLE_COUNTRY));
    }

    public ObservableList<BaseVehicle> getVehicleList() {
        return vehicleList;
    }
}
