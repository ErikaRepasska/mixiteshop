package transport.deliverytypes;

import exceptions.NegativeLoadException;
import list.BaseCell;
import transport.Order;

import java.util.ArrayList;

/**
 * Created by Erika on 29-Apr-17.
 */
public abstract class BaseVehicle extends BaseCell{

    public enum VehicleLocation {
        WEST("(západ)"), MIDDLE("(stred)"), EAST("(východ)"), WHOLE_COUNTRY("(celé Slovensko)");

        private final String vehicleLocation;
        VehicleLocation(final String vehicleLocation) {this.vehicleLocation = vehicleLocation;}
        @Override public String toString() {return vehicleLocation;}
    }

    private int currentLoad;
    private VehicleLocation vehicleLocation;
    private final ArrayList<Order> orderArrayList = new ArrayList<>();

    /**
     * Abstraktná trieda implementovaná v triedach extendujúcich z BaseVehicle
     * @return priraďuje čas, za ktorý bude doručená objednávka prostredníctvom daného spôsobu prepravy
     */
    public abstract int getDeliveryTime(); //day

    /**
     * Abstraktná trieda implementovaná v triedach extendujúcich z BaseVehicle
     * @return priraďuje maximálnu kapacitu vozila
     */
    public abstract int getMaxVehicleCapacity(); //gram

    BaseVehicle(final String name) {
        super(name);
    }

    public int getCurrentLoad() {
        return currentLoad;
    }

    public void setCurrentLoad(final int currentLoad) {
        if (currentLoad < 0) {
            throw new NegativeLoadException("Load cannot have negative value!");
        }
        this.currentLoad = currentLoad;
    }

    /**
     * Zisťuje možnosť pridania ďalšej objednávky do vozidla
     * @param order - objednávka, ktorú chceme pridať do vozidla
     * @return TRUE ak môže byť pridaná, inak FALSE
     */
    public boolean isEnoughSpaceForNextOrder(final Order order) {
        return currentLoad + order.getOrderWeightSummary() <= getMaxVehicleCapacity();
    }

    /**
     * Pridá objednávku do vozidla, ak je v ňom dostatok miesta
     * @param order Objednávka k pridaniu
     * @return TRUE ak bola pridaná a aktuálna hmotnosť nákladu v aute sa zvýši, inak FALSE
     */
    public boolean addNewOrderForDelivery(final Order order) {
        if (isEnoughSpaceForNextOrder(order)) {
            this.currentLoad += order.getOrderWeightSummary();
            return true;
        }

        return false;
    }

    public VehicleLocation getVehicleLocation() {
        return vehicleLocation;
    }

    public void setVehicleLocation(final VehicleLocation vehicleLocation) {
        this.vehicleLocation = vehicleLocation;
    }

    public String getAvailableCapacityAsString() {
        return String.valueOf(getMaxVehicleCapacity() - currentLoad);
    }

    public ArrayList<Order> getOrderArrayList() {
        return orderArrayList;
    }

    /**
     * Priraďuje objednávku danému vozidlu
     * @param order pridávaná objednávka
     */
    public void addOrderInArrayList(final Order order) {
        if (order != null) {
            orderArrayList.add(order);
        }
    }
}
