package transport;

import exceptions.NegativeTimeException;
import exceptions.NullCityException;
import exceptions.NullNameException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import packs.Pack;
import utils.Utils;

/**
 * Created by Erika on 26-Mar-17.
 */

public class Order {

    public enum OrderState {
        WAITING("Nevybavená"), DISPATCHED("Vybavená");

        private final String orderState;
        OrderState(final String orderState) {this.orderState = orderState;}
        @Override public String toString() {return orderState;}
    }

    private final ObservableList<Pack> packsList = FXCollections.observableArrayList();
    private long orderId;
    private String customerName;
    private City customerCity;
    private OrderState orderState;
    private int deliveryTime;

    public int getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(final int deliveryTime) {
        if (deliveryTime < 0) {
            throw new NegativeTimeException("Time cannot have negative value!");
        }
        this.deliveryTime = deliveryTime;
    }

    public Order() {
        orderState = OrderState.WAITING;
    }

    public void addPack(final Pack pack) {
        if (pack != null) {
            packsList.add(pack);
        }
    }

    public ObservableList<Pack> getPacksList() {
        return packsList;
    }

    /**
     * Iteruje balíkmi v objednávke a zvyšuje jej hmotnosť (ak nie je prázdna)
     * @return celková hmotnosť objednávky
     */
    public int getOrderWeightSummary() {
        int weight = 0;
        if (packsList.isEmpty()) {
            return weight;
        }

        for (final Pack pack : packsList) {
            weight += pack.getWeight();
        }
        return weight;
    }

    public String getOrderWeightSummaryAsString() {
        return String.format("%d%s", getOrderWeightSummary(), Utils.WEIGHT_UNIT);
    }

    /**
     * Iteruje balíkmi v objednávke a zvyšuje jej cenu (ak nie je prázdna)
     * @return celková cena objednávky
     */
    private double getOrderPriceSummary() {
        int price = 0;
        if (packsList.isEmpty()) {
            return price;
        }

        for (final Pack pack : packsList) {
            price += pack.getPrice();
        }
        return price;
    }

    public String getOrderPriceSummaryAsString() {
        return String.format("%.2f%s", getOrderPriceSummary(), Utils.CURRENCY_UNIT);
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(final long orderId) {
        this.orderId = orderId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(final String customerName) {
        if (customerName == null) {
            throw new NullNameException("Name cannot be null!");
        }
        this.customerName = customerName;
    }

    public City getCustomerCity() {
        return customerCity;
    }

    public void setCustomerCity(final City customerCity) {
        if (customerCity == null) {
            throw new NullCityException("City cannot be null!");
        }

        this.customerCity = customerCity;
    }

    public OrderState getOrderState() {
        return orderState;
    }

    public void setOrderState(final OrderState orderState) {
        this.orderState = orderState;
    }
}
