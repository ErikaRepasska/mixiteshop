package list;

import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;
import transport.Order;

/**
 * Trieda vykonávajúca výpis objednávok do ListView v GUI. Rieši logiku vypisovania z dvoch hľadísk:
 * objednávke, ktorá neprešla procesom logistiky priradí status "nevybavená"
 * objednávke, ktorá prešla procesom logistiky priradí status s dobou doručenia charakteristickou pre spôsob dopravy
 */
public class OrderCellFactory implements Callback<ListView<Order>, ListCell<Order>> {
    @Override
    public ListCell<Order> call(final ListView<Order> l){
        return new ListCell<Order>(){
            @Override
            protected void updateItem(final Order order, final boolean empty) {
                super.updateItem(order, empty);

                if (order == null) {
                    setText(null);
                    return;
                }

                String itemText = "Meno: " + order.getCustomerName() + " | Mesto: " + order.getCustomerCity().getName()
                        + " | Cena: " + order.getOrderPriceSummaryAsString() + " | Stav: " + order.getOrderState().toString();

                if (order.getOrderState() == Order.OrderState.DISPATCHED) {
                    itemText += " | Doba doručenia: " + order.getDeliveryTime() + " dni";
                }

                setText(itemText);
            }
        };
    }
}
