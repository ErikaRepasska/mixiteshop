package list;

public abstract class BaseCell {

    private final String name;

    protected BaseCell(final String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return name;
    }

    public String getName() {
        return name;
    }
}
