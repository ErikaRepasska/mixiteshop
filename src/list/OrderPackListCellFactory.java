package list;

import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;
import packs.Pack;

/**
 * Sprostredkuje výpis názvov objektov v prvkoch GUI
 */
public class OrderPackListCellFactory implements Callback<ListView<Pack>, ListCell<Pack>> {
    @Override
    public ListCell<Pack> call(final ListView<Pack> l){
        return new ListCell<Pack>(){
            @Override
            protected void updateItem(final Pack pack, final boolean empty) {
                super.updateItem(pack, empty);

                setText(pack != null ? pack.getOrderListDisplayName() : null);
            }
        };
    }
}
