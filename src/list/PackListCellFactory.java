package list;

import food.BaseFood;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

/**
 * Sprostredkuje výpis názvov balení v prvkoch používateľského rozhrania
 */
public class PackListCellFactory implements Callback<ListView<BaseFood>, ListCell<BaseFood>> {
    @Override
    public ListCell<BaseFood> call(final ListView<BaseFood> l){
        return new ListCell<BaseFood>(){
            @Override
            protected void updateItem(final BaseFood pack, final boolean empty) {
                super.updateItem(pack, empty);

                setText(pack != null ? pack.getNameWithWeight() : null);
            }
        };
    }
}
