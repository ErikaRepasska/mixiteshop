package list;

import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

/**
 * Táto trieda slúži na výpis názvu objektu do požadovaných prvkov v používateľskom rozhraní
 */
public class ListCellFactory implements Callback<ListView<BaseCell>, ListCell<BaseCell>> {
    @Override
    public ListCell<BaseCell> call(final ListView<BaseCell> l){
        return new ListCell<BaseCell>(){
            @Override
            protected void updateItem(final BaseCell pack, final boolean empty) {
                super.updateItem(pack, empty);

                setText(pack != null ? pack.getDisplayName() : null);
            }
        };
    }
}
