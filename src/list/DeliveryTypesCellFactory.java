package list;

import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;
import transport.deliverytypes.BaseVehicle;

/**
 * Táto trieda slúži na výpis objektu v príslušnom požadovanom tvare do ListView, nachádzajúci sa v GUI
 */
public class DeliveryTypesCellFactory implements Callback<ListView<BaseVehicle>, ListCell<BaseVehicle>> {
    @Override
    public ListCell<BaseVehicle> call(final ListView<BaseVehicle> l){
        return new ListCell<BaseVehicle>(){
            @Override
            protected void updateItem(final BaseVehicle baseVehicle, final boolean empty) {
                super.updateItem(baseVehicle, empty);

                setText(baseVehicle != null ? "Typ: " + baseVehicle.getDisplayName()
                        + " | Max. priestor: " + baseVehicle.getMaxVehicleCapacity()
                        + " | Voľný priestor: " + baseVehicle.getAvailableCapacityAsString() : null);
            }
        };
    }
}
