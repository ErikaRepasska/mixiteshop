package windows;

import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import list.OrderCellFactory;
import listeners.ListChangedListener;
import transport.Order;

/**
 * Created by Erika on 11-Apr-17.
 */
public class OrderStateWindow implements ListChangedListener {

    private final Stage orderStateWindow;
    private final ObservableList<Order> mOrderList;
    private final ListView<Order> orderListView = new ListView<>();

    public OrderStateWindow(final ObservableList<Order> orderList) {
        mOrderList = orderList;
        orderStateWindow = initGui();
    }

    /**
     * Nainicializuje okno so stavom objednávok
     * @return okno stavu objednávok
     */
    private Stage initGui() {
        final Stage orderStateWindow = new Stage();

        orderStateWindow.setTitle("STAV OBJEDNAVOK");

        final GridPane orderStateGrid = new GridPane();
        orderStateGrid.setAlignment(Pos.CENTER);
        orderStateGrid.setHgap(10);
        orderStateGrid.setVgap(10);
        orderStateGrid.setPadding(new Insets(25, 25, 25, 25));

        orderListView.setCellFactory(new OrderCellFactory());
        orderListView.setItems(mOrderList);
        orderListView.setPrefSize(600, 400);
        orderStateGrid.add(orderListView, 0, 0);

        orderStateWindow.setScene(new Scene(orderStateGrid, 650, 450));
        return orderStateWindow;
    }

    /**
     * Vykreslí okno stavu objednávok
     */
    public void show(){
        if (orderStateWindow != null) {
            orderStateWindow.show();
        }
    }

    /**
     * Ak došlo k zmene atribútu v liste
     * @return nová instancia listeneru
     */
    public ListChangedListener getListChangedListener() {
        return this;
    }

    @Override
    public void onListChanged() {
        if (orderListView != null) {
            orderListView.refresh();
        }
    }
}
