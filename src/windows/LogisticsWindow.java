package windows;

import exceptions.WrongOrderIndexException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;
import list.DeliveryTypesCellFactory;
import list.ListCellFactory;
import list.OrderCellFactory;
import listeners.ListChangedListener;
import transport.Order;
import transport.deliverytypes.BaseVehicle;
import transport.deliverytypes.Garage;

/**
 * Created by Erika on 29-Apr-17.
 */
public class LogisticsWindow {

    private final Callback listCellFactory = new ListCellFactory();
    private final Stage logisticsWindow;
    private final Garage mainGarage;
    private final ListView<BaseVehicle> vehicleListView = new ListView<>();
    private final ObservableList<Order> mOrderList;
    private final ObservableList<Order> mVisibleOrderList;
    private final ListView<Order> orderListView = new ListView<>();
    private final ListChangedListener mListChangedListener;
    private ComboBox vehicleComboBox;

    public LogisticsWindow(final Garage garage, final ObservableList<Order> orderList, final ListChangedListener listChangedListener) {
        mainGarage = garage;
        mOrderList = orderList;
        mListChangedListener = listChangedListener;
        mVisibleOrderList = FXCollections.observableArrayList();
        initVisibleOrderList();

        logisticsWindow = initGui();
    }

    /**
     * Nainicializuje list tých objednávok, ktoré ešte neprešli procesom logistiky - majú atribút "nevybavené"
     */
    private void initVisibleOrderList() {
        for (final Order order : mOrderList) {
            if (order.getOrderState() != Order.OrderState.DISPATCHED) {
                mVisibleOrderList.add(order);
            }
        }
    }

    /**
     * Nainicializuje okno logistiky
     * @return okno logistiky
     */
    private Stage initGui() {
        final Stage logisticsWindow = new Stage();

        logisticsWindow.setTitle("LOGISTIKA (zamestnanci)");

        final GridPane logisticsGrid = new GridPane();
        logisticsGrid.setAlignment(Pos.TOP_LEFT);
        logisticsGrid.setHgap(10);
        logisticsGrid.setVgap(10);
        logisticsGrid.setPadding(new Insets(25, 25, 25, 25));

        final Separator separator = new Separator();
        separator.setValignment(VPos.CENTER);
        logisticsGrid.add(separator, 0, 0, 8, 1);

        final Label dispatcherLabel = new Label("D I S P E C E R");
        logisticsGrid.add(dispatcherLabel, 0, 1, 1, 1);

        orderListView.setCellFactory(new OrderCellFactory());
        orderListView.setItems(mVisibleOrderList);
        orderListView.setPrefSize(450, 300);
        logisticsGrid.add(orderListView, 0, 2, 2, 1);

        vehicleListView.setCellFactory(new DeliveryTypesCellFactory());
        vehicleListView.setItems(mainGarage.getVehicleList());
        vehicleListView.setPrefSize(450, 300);
        logisticsGrid.add(vehicleListView, 3, 2, 2, 1);

        final Button confirmButton = new Button("Priradit zvolenu objednavku");
        confirmButton.setOnAction(event -> addIntoVehicle(orderListView.getSelectionModel().getSelectedItem(), vehicleListView.getSelectionModel().getSelectedItem()));
        logisticsGrid.add(confirmButton, 0, 3);

        final Separator separator2 = new Separator();
        separator2.setValignment(VPos.CENTER);
        logisticsGrid.add(separator2, 0, 4, 8, 1);

        final Label driverLabel = new Label("P R E P R A V C A");
        logisticsGrid.add(driverLabel, 0, 5, 4, 1);

        vehicleComboBox = new ComboBox(mainGarage.getVehicleList());
        setFactory(vehicleComboBox);
        vehicleComboBox.getSelectionModel().selectFirst();
        logisticsGrid.add(new Label("Zvolte prepravu, za ktoru zodpovedate:"), 0, 6, 2, 1);
        logisticsGrid.add(vehicleComboBox, 0, 7);
        final Button confirmButton2 = new Button("VYBAVIT");
        confirmButton2.setOnAction(event -> clearOrderList((BaseVehicle) vehicleComboBox.getValue()));
        logisticsGrid.add(confirmButton2, 1, 7);

        final Separator separator3 = new Separator();
        separator3.setValignment(VPos.CENTER);
        logisticsGrid.add(separator3, 0, 8, 8, 1);

        logisticsWindow.setScene(new Scene(logisticsGrid, 1000, 500));
        return logisticsWindow;
    }

    /**
     * Vykreslí okno logistiky
     */
    public void show(){
        if (logisticsWindow != null) {
            logisticsWindow.show();
        }
    }

    /**
     * Priradenie objednávky vozidlu, ak je v ňom dostatok miesta
     * @param order - objednávka, ktorú chceme priradiť
     * @param vehicle - vozidlo, ktorému ju chceme priradiť
     */
    private void addIntoVehicle(final Order order, final BaseVehicle vehicle) {
        if (order != null && vehicle != null) {
            if (vehicle.isEnoughSpaceForNextOrder(order)) {
                updateMainOrderList(order);

                vehicle.addOrderInArrayList(order);
                vehicle.addNewOrderForDelivery(order);
                order.setDeliveryTime(vehicle.getDeliveryTime());
                vehicleListView.refresh();
            } else {
                notEnoughSpaceForNextOrderDialog();
            }
        } else {
            wrongSelectionDialog();
        }
    }

    /**
     * Aktualizuje list objednávok - odstráni z listu nevybavených objednávok tú, ktorá bola práve vybavená
     * a nastaví jej nový status s dobou doručenia
     * Upovedomí sa ListChangedListener o tom, že bol zmenený atribút v liste
     * @param visibleOrder zobrazená objednávka
     */
    private void updateMainOrderList(final Order visibleOrder) {
        if (!orderListView.getItems().remove(visibleOrder)) {
            throw new WrongOrderIndexException("Given order is not a part of this list!");
        }

        for (final Order order : mOrderList) {
            if (order.equals(visibleOrder)) {
                order.setOrderState(Order.OrderState.DISPATCHED);
                break;
            }
        }

        if (mListChangedListener != null) {
            mListChangedListener.onListChanged();
        }
    }

    /**
     * Nastaví opäť kapacitu vozidla na maximálnu a vymaže objednávku z eshopu ak bola vodičom doručená zákazníkovi
     * @param baseVehicle - vozidlo, ktorého vodič potvrdil doručenie tovaru
     */
    private void clearOrderList(final BaseVehicle baseVehicle) {
        if (baseVehicle != null) {
            for (final Order order : baseVehicle.getOrderArrayList()) {
                mOrderList.remove(order);
            }
            baseVehicle.getOrderArrayList().clear();
            baseVehicle.setCurrentLoad(0);
            vehicleListView.refresh();
        }
    }

    /**
     * Zabezpečuje správne vypisovanie do ComboBox-u
     * @param comboBox do ktorého chceme vypisovať
     */
    private void setFactory(final ComboBox comboBox) {
        comboBox.setButtonCell((ListCell) listCellFactory.call(null));
        comboBox.setCellFactory(listCellFactory);
    }

    /**
     * Dialog informujúci o správnom priraďovaní objednávok.
     */
    private void notEnoughSpaceForNextOrderDialog() {
        final Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Info");
        alert.setHeaderText("Vo vozdile nie je dostatok miesta pre pridanie dalsej objednavky :-(");
        alert.show();
    }

    /**
     * Dialog informujúci o správnom priraďovaní objednávok.
     */
    private void wrongSelectionDialog() {
        final Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Info");
        alert.setHeaderText("Pre priradenie objednávky do vozidla musia byt oznacene obe polozky :-)");
        alert.show();
    }
}
