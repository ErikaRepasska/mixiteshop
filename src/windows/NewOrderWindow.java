package windows;

import food.BaseFood;
import interfaces.CurrentPackInterface;
import interfaces.NewOrderInterface;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;
import list.ListCellFactory;
import list.OrderPackListCellFactory;
import listeners.WindowCloseListener;
import packs.Pack;
import storage.Storage;
import transport.City;
import transport.DeliveryPoints;
import transport.Order;

/**
 * Created by Erika on 14-Mar-17.
 */
public class NewOrderWindow implements WindowCloseListener, CurrentPackInterface {

    private final Callback listCellFactory;
    private final Storage mainStorage;
    private final DeliveryPoints listOfDeliveryPoints;
    private final NewOrderInterface newOrderInterface;
    private final Stage orderWindow;
    private CurrentPackWindow currentPackWindow;
    private Button addNewPackButton, addBaseFoodButton, addDriedFruitButton, addNutButton, addAddOnButton, completeOrderButton;
    private ComboBox packComboBox, baseFoodComboBox, driedFruitComboBox, nutComboBox, addOnComboBox, deliveryPointsComboBox;
    private TextField orderPriceTextField, orderWeightTextField, contactNameTextField;

    private final Order order;
    private final ListView<Pack> packListView = new ListView<>();

    public NewOrderWindow(final Storage storage, final DeliveryPoints deliveryPoints, final NewOrderInterface newOrderInterface) {
        this.newOrderInterface = newOrderInterface;

        mainStorage = storage;
        listOfDeliveryPoints = deliveryPoints;
        listCellFactory = new ListCellFactory();
        order = new Order();

        orderWindow = initGui();
    }

    /**
     * Nainicializuje okno pre novú objednávku
     * @return okno pre novú objednávku
     */
    @SuppressWarnings("unchecked")
    private Stage initGui() {
        final Stage orderWindow = new Stage();
        orderWindow.setTitle("NOVA OBJEDNAVKA");

        final GridPane newOrderGrid = new GridPane();
        newOrderGrid.setAlignment(Pos.TOP_LEFT);
        newOrderGrid.setHgap(10);
        newOrderGrid.setVgap(10);
        newOrderGrid.setPadding(new Insets(25, 25, 25, 25));

        /* BALENIA */
        packComboBox = new ComboBox(mainStorage.getAvailablePacksList());
        setFactory(packComboBox);
        packComboBox.getSelectionModel().selectFirst();
        newOrderGrid.add(new Label("Vyberte si velkost balenia:"), 0, 0);
        newOrderGrid.add(packComboBox, 0, 1);
        addNewPackButton = new Button("Pridaj balenie!");
        addNewPackButton.setOnAction(event -> createNewPackWindow());
        newOrderGrid.add(addNewPackButton, 1, 1);

        final Separator separator = new Separator();
        separator.setValignment(VPos.CENTER);
        newOrderGrid.add(separator, 0, 2, 4, 1);

        /* BUTTONY */
        addBaseFoodButton = new Button("Pridaj zaklad");
        addDriedFruitButton = new Button("Pridaj ovocie");
        addNutButton = new Button("Pridaj orechy");
        addAddOnButton = new Button("Pridaj doplnok");

        /* ZAKLAD */
        baseFoodComboBox = new ComboBox(mainStorage.getBaselineFoodList());
        setFactory(baseFoodComboBox);
        baseFoodComboBox.getSelectionModel().selectFirst();
        newOrderGrid.add(new Label("Zaklad:"), 0, 3);
        newOrderGrid.add(baseFoodComboBox, 0, 4);
        addBaseFoodButton.setOnAction(event -> addFoodToCurrentPackWidow(baseFoodComboBox));
        newOrderGrid.add(addBaseFoodButton, 0, 5);

        /* SUSENE OVOCIE */
        driedFruitComboBox = new ComboBox(mainStorage.getDriedFruitList());
        setFactory(driedFruitComboBox);
        driedFruitComboBox.getSelectionModel().selectFirst();
        newOrderGrid.add(new Label("Susene ovocie:"), 1, 3);
        newOrderGrid.add(driedFruitComboBox, 1, 4);
        addDriedFruitButton.setOnAction(event -> addFoodToCurrentPackWidow(driedFruitComboBox));
        newOrderGrid.add(addDriedFruitButton, 1, 5);

        /* ORECHY */
        nutComboBox = new ComboBox(mainStorage.getNutsList());
        setFactory(nutComboBox);
        nutComboBox.getSelectionModel().selectFirst();
        newOrderGrid.add(new Label("Orechy:"), 2, 3);
        newOrderGrid.add(nutComboBox, 2, 4);
        addNutButton.setOnAction(event -> addFoodToCurrentPackWidow(nutComboBox));
        newOrderGrid.add(addNutButton, 2, 5);

        /* DOPLNKY */
        addOnComboBox = new ComboBox(mainStorage.getAddOnsList());
        setFactory(addOnComboBox);
        addOnComboBox.getSelectionModel().selectFirst();
        newOrderGrid.add(new Label("Doplnky:"), 3, 3);
        newOrderGrid.add(addOnComboBox, 3, 4);
        addAddOnButton.setOnAction(event -> addFoodToCurrentPackWidow(addOnComboBox));
        newOrderGrid.add(addAddOnButton, 3, 5);

        final Separator separator2 = new Separator();
        separator2.setValignment(VPos.CENTER);
        newOrderGrid.add(separator2, 0, 6, 4, 1);

        /* PACK LIST */
        newOrderGrid.add(new Label("Obsah kosika"), 0, 7);
        packListView.setCellFactory(new OrderPackListCellFactory());
        packListView.setItems(order.getPacksList());
        packListView.setPrefHeight(140);
        newOrderGrid.add(packListView, 0, 8, 4, 1);

        newOrderGrid.add(new Label("Cena objednavky:"), 0, 9);
        orderPriceTextField = new TextField(order.getOrderPriceSummaryAsString());
        newOrderGrid.add(orderPriceTextField, 0, 10);

        newOrderGrid.add(new Label("Hmotnost objednavky:"), 1, 9);
        orderWeightTextField = new TextField(order.getOrderWeightSummaryAsString());
        newOrderGrid.add(orderWeightTextField, 1, 10);

        final Separator separator3 = new Separator();
        separator3.setValignment(VPos.CENTER);
        newOrderGrid.add(separator3, 0, 11, 4, 1);

        /* KONTAKTNE UDAJE */
        deliveryPointsComboBox = new ComboBox(listOfDeliveryPoints.getDeliveryPoints());
        setFactory(deliveryPointsComboBox);
        deliveryPointsComboBox.getSelectionModel().selectFirst();
        newOrderGrid.add(new Label("Kontaktne udaje"), 0, 12);
        newOrderGrid.add(new Label("Meno a priezvisko:"), 0, 13);
        contactNameTextField = new TextField();
        newOrderGrid.add(contactNameTextField, 1, 13);
        newOrderGrid.add(new Label("Miesto dorucenia:"), 0, 14);
        deliveryPointsComboBox.setPrefWidth(150);
        newOrderGrid.add(deliveryPointsComboBox, 1, 14);

        final Separator separator4 = new Separator();
        separator4.setValignment(VPos.CENTER);
        newOrderGrid.add(separator4, 0, 15, 4, 1);

        completeOrderButton = new Button("NAMIXOVAT OBJEDNAVKU! :-)");
        completeOrderButton.setPrefSize(300, 100);
        completeOrderButton.setDisable(true);
        completeOrderButton.setOnAction(event -> completeOrder(orderWindow));
        newOrderGrid.add(completeOrderButton, 1, 16, 2, 1);

        orderWindow.setScene(new Scene(newOrderGrid, 600, 600));
        return orderWindow;
    }

    /**
     * Aktualizuje informácie objednávky - cenu a váhu
     */
    private void updateOrderInfo() {
        if (orderPriceTextField != null && orderWeightTextField != null) {
            orderPriceTextField.setText(order.getOrderPriceSummaryAsString());
            orderWeightTextField.setText(order.getOrderWeightSummaryAsString());
        }
    }

    /**
     * Vytvorí okno pre tvorbu nového balenia
     */
    private void createNewPackWindow() {
        final Pack comboBoxPack = (Pack) packComboBox.getValue();
        currentPackWindow = new CurrentPackWindow(new Pack(comboBoxPack.getName(), comboBoxPack.getPackSize()), this, this);
        currentPackWindow.show();
        addNewPackButton.setDisable(true);
    }

    /**
     * Vykreslí okno pre novú objednávku
     */
    public void show(){
        if (orderWindow != null) {
            orderWindow.show();
        }
    }

    /**
     * Pridá vybranú surovinu z ComboBox-u do balenia
     * @param comboBox z ktorého sa surovina vyberá
     */
    private void addFoodToCurrentPackWidow(final ComboBox comboBox) {
        if (currentPackWindow != null) {
            currentPackWindow.addFood((BaseFood) comboBox.getValue());
            checkIfCanBeAddedAnotherFood();
        }
    }

    /**
     * Zisťuje či môže byť pridaná surovina do balenia
     * Ak nie, nastaví tlačidlo pre daný typ suroviny aby sa naň nedalo kliknúť
     */
    private void checkIfCanBeAddedAnotherFood() {
        if (currentPackWindow != null) {
            final Pack currentPack = currentPackWindow.getPack();
            if (!currentPack.canBeAddedAnotherFood(((BaseFood) baseFoodComboBox.getValue()))) {
                addBaseFoodButton.setDisable(true);
            }
            if (!currentPack.canBeAddedAnotherFood(((BaseFood) driedFruitComboBox.getValue()))) {
                addDriedFruitButton.setDisable(true);
            }
            if (!currentPack.canBeAddedAnotherFood(((BaseFood) nutComboBox.getValue()))) {
                addNutButton.setDisable(true);
            }
            if (!currentPack.canBeAddedAnotherFood(((BaseFood) addOnComboBox.getValue()))) {
                addAddOnButton.setDisable(true);
            }
        }
    }

    /**
     * Dokončenie objednávky, ak boli vyplnené všetky potrebné informácie
     * @param orderWindow okno aktuálnej objednávky
     */
    private void completeOrder(final Stage orderWindow) {
        if (contactNameTextField != null && contactNameTextField.getText() != null
                && contactNameTextField.getText().trim().isEmpty()) {
            showNameRequiredDialog();
            return;
        }

        if (newOrderInterface != null && !order.getPacksList().isEmpty()) {
            order.setOrderId(System.currentTimeMillis());
            order.setCustomerName(contactNameTextField.getText().trim());
            order.setCustomerCity((City) deliveryPointsComboBox.getValue());
            newOrderInterface.addOrder(order);
            orderWindow.close();
        }
    }

    /**
     * Dialog informujúci o nutnosti vyplnenia mena zákazníka
     */
    private void showNameRequiredDialog() {
        final Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Info");
        alert.setHeaderText("Pre dokoncenie objednavky, vyplnte prosim Vase meno");
        alert.show();
    }

    @SuppressWarnings("unchecked")
    private void setFactory(final ComboBox comboBox) {
        comboBox.setButtonCell((ListCell) listCellFactory.call(null));
        comboBox.setCellFactory(listCellFactory);
    }

    @Override
    public void onCurrentPackCompleted(final Pack pack) {
        if (pack != null && !pack.getFoodList().isEmpty()) {
            order.addPack(pack);
            updateOrderInfo();
            completeOrderButton.setDisable(false);
        }
    }

    @Override
    public void onChildWindowClosed() {
        if (addNewPackButton != null) {
            addNewPackButton.setDisable(false);
            addBaseFoodButton.setDisable(false);
            addDriedFruitButton.setDisable(false);
            addNutButton.setDisable(false);
            addAddOnButton.setDisable(false);
            currentPackWindow = null;
        }
    }
}
