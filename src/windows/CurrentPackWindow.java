package windows;

import food.BaseFood;
import interfaces.CurrentPackInterface;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import list.PackListCellFactory;
import listeners.WindowCloseListener;
import packs.Pack;

/**
 * Created by Erika on 14-Mar-17.
 */
public class CurrentPackWindow implements EventHandler<WindowEvent> {

    private static final int TOLERANCE_LIMIT = 100;

    private final WindowCloseListener windowCloseListener;
    private final CurrentPackInterface currentPackInterface;
    private final ListView<BaseFood> currentPackListView = new ListView<>();
    private final Stage packWindow;
    private final Pack pack;
    private TextField priceTextField;
    private TextField weightTextField;
    private Button confirmButton;

    public CurrentPackWindow(final Pack pack, final WindowCloseListener windowCloseListener,
                             final CurrentPackInterface currentPackInterface) {
        this.pack = pack;
        this.windowCloseListener = windowCloseListener;
        this.currentPackInterface = currentPackInterface;

        packWindow = initGui(pack);
    }

    /**
     * Nainicializuje nové okno z "NewOrderWindow" pre vytváranie nového balenia
     * @param pack - balík, ktorý vytvárame
     * @return okno pre vytváranie nového balenia
     */
    private Stage initGui(final Pack pack) {
        final Stage packWindow = new Stage();
        packWindow.setTitle(pack.getDisplayName());

        final GridPane currentPackGridLayout = new GridPane();
        currentPackGridLayout.setAlignment(Pos.TOP_CENTER);
        currentPackGridLayout.setHgap(5);
        currentPackGridLayout.setVgap(5);
        currentPackGridLayout.setPadding(new Insets(25, 25, 25, 25));

        currentPackListView.setCellFactory(new PackListCellFactory());
        currentPackListView.setItems(pack.getFoodList());
        currentPackGridLayout.add(currentPackListView, 0, 0);

        final GridPane infoGridLayout = new GridPane();

        infoGridLayout.add(new Label("Cena:"), 0, 0);
        priceTextField = new TextField(pack.getPriceAsString());
        infoGridLayout.add(priceTextField, 0, 1);

        infoGridLayout.add(new Label("Hmotnost:"), 1, 0);
        weightTextField = new TextField(pack.getWeightAsString());
        infoGridLayout.add(weightTextField, 1, 1);

        confirmButton = new Button("Dokončiť!");
        confirmButton.setDisable(true);
        confirmButton.setOnAction(event -> {
            if (currentPackInterface != null) currentPackInterface.onCurrentPackCompleted(pack);
            onWindowClosed();
            packWindow.close();
        });
        infoGridLayout.add(confirmButton, 0, 2);

        currentPackGridLayout.add(infoGridLayout, 0, 1);

        packWindow.setScene(new Scene(currentPackGridLayout, 400, 400));
        packWindow.setOnCloseRequest(this);
        return packWindow;
    }

    /**
     * Vykreslí okno
     */
    public void show(){
        if (packWindow != null) {
            packWindow.show();
        }
    }

    /**
     * Pridávanie suroviny do balenia, ak je balenie vytvorené + s každým pridaním aktualizuje jeho obsah
     * @param food pridávaná surovina
     */
    public void addFood(final BaseFood food) {
        if (currentPackListView != null) {
            currentPackListView.getItems().add(food);
            updatePackWindowInfo();
        }
    }

    public Pack getPack() {
        return pack;
    }

    /**
     * Aktualizuje informácie o balení
     * Vypisuje jeho celkovú cenu a váhu
     *
     * Ak je jeho aktuálna váha menšia ako maximálna váha - určitá tolerancia, konfirmačné tlačidlo na dokončenie
     * balenia nie je možné stlačiť.
     * Po dosiahnutí minimálnej požadovanej váhy pre dané balenie sa tlačidlo zviditeľní a je možné dokončiť
     * pridávanie do balenia.
     */
    private void updatePackWindowInfo() {
        if (priceTextField != null && weightTextField != null) {
            priceTextField.setText(pack.getPriceAsString());
            weightTextField.setText(pack.getWeightAsString());
        }

        if (pack.getWeight() <= pack.getMaxWeight() - TOLERANCE_LIMIT) {
            confirmButton.setDisable(true);
        } else {
            confirmButton.setDisable(false);
        }
    }

    @Override
    public void handle(final WindowEvent event) {
        if (event.getEventType() == WindowEvent.WINDOW_CLOSE_REQUEST) {
            onWindowClosed();
        }
    }

    private void onWindowClosed() {
        if (windowCloseListener != null) {
            windowCloseListener.onChildWindowClosed();
        }
    }
}
