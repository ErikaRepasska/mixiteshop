package storage;

import food.addon.*;
import food.baseline.*;
import food.driedfruit.*;
import food.nut.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import packs.Pack;

/**
 * Trieda sprostredkujúca napĺňanie a vypisovanie listov balení a surovín (základ, sušené ovocie, oriešky, doplnky)
 * Reprezentuje sklad nášho eshopu
 */
public class Storage {

    private final ObservableList<Pack> packsList = FXCollections.observableArrayList();
    private final ObservableList<BaselineFood> baselineFoodList = FXCollections.observableArrayList();
    private final ObservableList<DriedFruit> driedFruitList = FXCollections.observableArrayList();
    private final ObservableList<Nut> nutsList = FXCollections.observableArrayList();
    private final ObservableList<AddOn> addOnsList = FXCollections.observableArrayList();

    public Storage() {
        preparePackList();
        fillBaseLineFoodList();
        fillDriedFruitList();
        fillNutsList();
        fillAddOnsList();
    }

    private void preparePackList() {
        packsList.add(new Pack("S", Pack.Size.SMALL));
        packsList.add(new Pack("M", Pack.Size.MEDIUM));
        packsList.add(new Pack("L", Pack.Size.LARGE));
        packsList.add(new Pack("XL", Pack.Size.EXTRA_LARGE));
    }

    private void fillBaseLineFoodList() {
        baselineFoodList.add(new OatFlakes());
        baselineFoodList.add(new CornFlakes());
        baselineFoodList.add(new RiceFlakes());
        baselineFoodList.add(new SoyaFlakes());
        baselineFoodList.add(new WheatFlakes());
    }

    private void fillDriedFruitList() {
        driedFruitList.add(new Banana());
        driedFruitList.add(new Blueberry());
        driedFruitList.add(new Cherry());
        driedFruitList.add(new Pineapple());
        driedFruitList.add(new Strawberry());
    }

    private void fillNutsList() {
        nutsList.add(new Almond());
        nutsList.add(new Cashew());
        nutsList.add(new Hazelnut());
        nutsList.add(new Peanut());
    }

    private void fillAddOnsList() {
        addOnsList.add(new ChocoCoffee());
        addOnsList.add(new ChocoNuts());
        addOnsList.add(new Cinnamon());
        addOnsList.add(new Coconut());
        addOnsList.add(new DarkChoco());
        addOnsList.add(new MilkChoco());
    }

    public ObservableList<Pack> getAvailablePacksList() {
        return packsList;
    }

    public ObservableList<BaselineFood> getBaselineFoodList() {
        return baselineFoodList;
    }

    public ObservableList<DriedFruit> getDriedFruitList() {
        return driedFruitList;
    }

    public ObservableList<Nut> getNutsList() {
        return nutsList;
    }

    public ObservableList<AddOn> getAddOnsList() {
        return addOnsList;
    }
}
