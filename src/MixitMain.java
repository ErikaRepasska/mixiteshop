import interfaces.NewOrderInterface;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import listeners.ListChangedListener;
import storage.Storage;
import transport.DeliveryPoints;
import transport.Order;
import transport.deliverytypes.Garage;
import windows.LogisticsWindow;
import windows.NewOrderWindow;
import windows.OrderStateWindow;

/**
 * Created by Erika on 14-Mar-17.
 */
public class MixitMain extends Application implements NewOrderInterface {

    private ObservableList<Order> orderList = FXCollections.observableArrayList();
    private final DeliveryPoints deliveryPoints;
    private final Storage mainStorage;
    private final Garage mainGarage;

    public MixitMain() {
        mainStorage = new Storage();
        mainGarage = new Garage();
        deliveryPoints = new DeliveryPoints();
        orderList = FXCollections.observableArrayList();
    }

    @Override
    public void start(final Stage primaryStage) throws Exception {
        primaryStage.setTitle("Eshop");
        primaryStage.setScene(new Scene(createGridPane()));
        primaryStage.show();

        primaryStage.setOnCloseRequest(event -> Platform.exit());
    }

    private HBox createHBox() {
        final HBox hbox = new HBox();
        hbox.setPadding(new Insets(10, 55, 10, 0));
        hbox.setSpacing(10);

        final Button newOrderButton = new Button("Nová objednávka");
        newOrderButton.setPrefSize(120, 20);
        newOrderButton.setOnAction(event -> new NewOrderWindow(mainStorage, deliveryPoints, this).show());

        final OrderStateWindow orderStateWindow = new OrderStateWindow(orderList);
        final ListChangedListener listChangedListener = orderStateWindow.getListChangedListener();
        final Button ordersStateButton = new Button("Stav objednávek");
        ordersStateButton.setPrefSize(120, 20);
        ordersStateButton.setOnAction(event -> orderStateWindow.show());

        final Button logisticsButton = new Button("Logistika");
        logisticsButton.setPrefSize(120, 20);
        logisticsButton.setOnAction(event -> new LogisticsWindow(mainGarage, orderList, listChangedListener).show());

        hbox.getChildren().addAll(newOrderButton, ordersStateButton, logisticsButton);
        return hbox;
    }

    private GridPane createGridPane() {
        final GridPane welcomeGridPane = new GridPane();
        welcomeGridPane.setAlignment(Pos.TOP_CENTER);
        welcomeGridPane.setHgap(5);
        welcomeGridPane.setVgap(5);
        welcomeGridPane.setPadding(new Insets(25, 0, 25, 55));

        welcomeGridPane.add(new ImageView("https://www.mixit.sk/assets/logo-c27df551eb6687f74d4a87449bf37703ad465daf382ad8b4025c9b3d6b8ff4e0.png"), 0 ,0 );
        welcomeGridPane.add(createHBox(), 0 , 1);

        return  welcomeGridPane;
    }

    @Override
    public void addOrder(final Order order) {
        orderList.add(order);
    }

    @Override
    public Order getOrder(final int position) {
        if ((orderList.size() - 1) >= position) {
            return orderList.get(position);
        }
        return null;
    }
}
