package interfaces;

import packs.Pack;

/**
 * Rozhranie sledujúce správania balení
 */
public interface CurrentPackInterface {
    /**
     * Proces mixovania nového balenie bol dokončený a balenie bolo úspešne vytvorené
     * @param pack balenie, ktoré bolo práve vytvorené
     */
    void onCurrentPackCompleted(final Pack pack);
}
