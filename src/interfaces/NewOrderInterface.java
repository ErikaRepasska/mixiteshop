package interfaces;

import transport.Order;

/**
 * Rozhranie sledujúce správanie objednávok
 */
public interface NewOrderInterface {
    /**
     * Nová objednávka bola vytvorená
     * @param order novo-vytvorená objednávka
     */
    void addOrder(final Order order);
    Order getOrder(final int position);
}
