package packs;

import food.BaseFood;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import list.BaseCell;
import utils.Utils;

/**
 * Created by Erika on 14-Mar-17.
 */
public class Pack extends BaseCell {

    public enum Size {
        SMALL(500), MEDIUM(750), LARGE(1000), EXTRA_LARGE(1500);

        private final int sizeValue;
        Size(final int sizeValue) {this.sizeValue = sizeValue;}
        public int getSizeValue() {return sizeValue;}
    }

    private final ObservableList<BaseFood> foodList = FXCollections.observableArrayList();
    private final Size packSize;

    public Pack(final String name, final Size packSize) {
        super(name);
        this.packSize = packSize;
    }

    public Size getPackSize() {
        return packSize;
    }

    public int getMaxWeight() {
        return packSize.getSizeValue();
    }

    public int getWeight() {
        int weight = 0;
        if (foodList.isEmpty()) {
            return weight;
        }

        for (final BaseFood baseFood : foodList) {
            weight += baseFood.getWeight();
        }
        return weight;
    }

    public String getWeightAsString() {
        return String.format("%d%s", getWeight(), Utils.WEIGHT_UNIT);
    }

    public double getPrice() {
        double price = 0;
        if (foodList.isEmpty()) {
            return price;
        }

        for (final BaseFood baseFood : foodList) {
            price += baseFood.getPrice();
        }
        return price;
    }

    public String getPriceAsString() {
        return String.format("%.2f%s", getPrice(), Utils.CURRENCY_UNIT);
    }

    /**
     * Zisťuje možnosť pridania ďalšej suroviny do balenia
     * @param food - surovina, ktorú chceme pridať do balenia
     * @return TRUE ak môže byť pridaná, inak vracia FALSE
     */
    public boolean canBeAddedAnotherFood(final BaseFood food) {
        return food.getWeight() <= packSize.getSizeValue() - getWeight();
    }

    public ObservableList<BaseFood> getFoodList() {
        return foodList;
    }

    @Override
    public String getDisplayName() {
        return String.format("%s (%d%s)", getName(), packSize.getSizeValue(), Utils.WEIGHT_UNIT);
    }

    /**
     * Výpis balenia v požadovanom formáte pre prehľad novo-vytváranej objednávky
     * @return balenie vo formáte "názov balenia, jeho cena a hmotnosť"
     */
    public String getOrderListDisplayName() {
        return String.format("%s / %s, %s", getDisplayName(), getPriceAsString(), getWeightAsString());
    }
}
