package exceptions;

/**
 * Created by Erika on 10-May-17.
 */
public class WrongOrderIndexException extends ArrayIndexOutOfBoundsException {

    public WrongOrderIndexException(String s) {
        super(s);
    }
}
