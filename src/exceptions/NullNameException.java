package exceptions;

/**
 * Created by Erika on 10-May-17.
 */
public class NullNameException extends NullPointerException {

    public NullNameException(String s) {
        super(s);
    }
}
