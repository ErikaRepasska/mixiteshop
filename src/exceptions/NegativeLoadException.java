package exceptions;

/**
 * Created by Erika on 10-May-17.
 */
public class NegativeLoadException extends IllegalArgumentException {

    public NegativeLoadException(String s) {
        super(s);
    }
}
