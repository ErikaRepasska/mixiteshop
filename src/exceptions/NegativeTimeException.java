package exceptions;

/**
 * Created by Erika on 10-May-17.
 */
public class NegativeTimeException extends IllegalArgumentException {

    public NegativeTimeException(String s) {
        super(s);
    }
}
