package exceptions;

/**
 * Created by Erika on 10-May-17.
 */
public class NullCityException extends NullPointerException {

    public NullCityException(String s) {
        super(s);
    }
}
